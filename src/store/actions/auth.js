import axios from 'axios';

import * as actionTypes from './actionTypes';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START,
        error: null
    };
};

export const authSuccess = (token, userId, refreshToken) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        idToken: token,
        userId: userId,
        error: null,
        refreshToken: refreshToken
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};

export const logout = () => {
    localStorage.removeItem('_token');
    localStorage.removeItem('expirationDate');
    localStorage.removeItem('userId');
    localStorage.removeItem('_refreshToken');

    return {
        type: actionTypes.AUTH_LOGOUT
    }
}

export const checkAuthTimeout = ( expirationTime ) => dispatch => {
    // console.log('debe expirar en ', expirationTime);
    
    setTimeout(() => {
        dispatch(logout() )
        console.log('jue el timeout');
        
    }, expirationTime * 100);
}

export const auth = (email, password, isSignup) => dispatch =>{
        dispatch(authStart());
        const authData = {
            email: email,
            password: password,
            returnSecureToken: true
        };
        let url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyAFi-Tm603OZoRjGbMJiEXj4gZhbvfacjs';
        if ( !isSignup ) {
            url = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyAFi-Tm603OZoRjGbMJiEXj4gZhbvfacjs';
        }
        axios.post(url, authData)
        .then(response => {
            // console.log(response);
            setTimeout(() => {
                const expirationDate = new Date( new Date().getTime()  + (response.data.expiresIn * 1000));
                console.log(expirationDate);
                
                localStorage.setItem('_token', response.data.idToken);
                localStorage.setItem('expirationDate', expirationDate);
                localStorage.setItem('userId', response.data.localId);
                localStorage.setItem('_refreshToken', response.data.refreshToken);
                dispatch (authSuccess(response.data.idToken, response.data.localId, response.data.refreshToken ));
                dispatch(checkAuthTimeout(response.data.expiresIn))
            }, 1000);
        })
        .catch(err => {
            // console.log(err.response.data.error);
            dispatch(authFail(err.response.data.error));
        });
};

export const setAuthRedirectPath = ( path )  => {
    return {
        type: actionTypes.SET_AUTH_REDIRECT_PATH, 
        path: path
    }
}

export const authCheckState = ( ) => dispatch => {
    const token = localStorage.getItem('_token');
    // console.log('authCheckState', token);
    
    if ( !!token ) {
        const expirationDate    =  new Date(localStorage.getItem('expirationDate'));
        const refreshToken      = localStorage.getItem('_refreshToken');
        
        if ( expirationDate <= new Date() ) {
            // console.log('Expiro');
            
            dispatch(logout())
        } else {
            // console.log('No expiro',expirationDate.getTime(), new Date().getTime());
            
            const userId    = localStorage.getItem('userId');
            dispatch(authSuccess(token, userId, refreshToken));
            dispatch( checkAuthTimeout( (expirationDate.getTime() - new Date().getTime())))
        }

    } else {
        dispatch(logout());
    }
}